import React, { useEffect, useState } from 'react';
import {
  Text, View, Image,
  StyleSheet, FlatList, ActivityIndicator
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { Card } from 'react-native-shadow-cards';


const App = () => {

  const [isLoading, setLoading] = useState(true);
  const [data, setData] = useState([]);

  const getMovies = async () => {
    try {
      const response = await fetch('https://tinyfac.es/api/data?limit=50&quality=0');
      const json = await response.json();
      setData(json);
    } catch (error) {
      console.error(error);
    } finally {
      setLoading(false);
    }
  }

  useEffect(() => {
    getMovies();
  }, []);

  return (

    <View style={styles.container}>
      <View>
        <Text style={styles.icon1} ><Icon name='arrow-back-ios' color={'black'} size={30} /></Text>
        <Text style={styles.text1}>Cleaning Services</Text>
        <Text style={styles.icon2}><Icon name='notifications-none' color={'black'} size={30} /></Text>
      </View>
      <View style={styles.one} >
        <Image source={{ uri: 'https://www.ecomaids.com/camphill-pa/wp-content/uploads/sites/41/2021/01/header_bg-32-1.jpg' }} style={styles.image1}></Image>
        <Image source={{ uri: 'https://media.istockphoto.com/photos/housework-is-not-just-for-women-picture-id904149352?k=20&m=904149352&s=612x612&w=0&h=-ejW3Ym__8FnPOmK9SsDZxOB3fExIe5SG7AvbwDQUHQ=' }} style={styles.image2}></Image>
      </View>
      <View style={styles.two}>
        <Text style={styles.text4}>Kitchen Cleaner</Text>
        <Text style={styles.text5}>Room Cleaner</Text></View>
      <View style={styles.three} >
        <Text style={styles.text2}>Experts</Text>
        <Text style={styles.text3}>View More</Text>
      </View>
      <View  style={styles.four} >
        {isLoading ? <ActivityIndicator /> : (
          <FlatList
            data={data}
            renderItem={({ item }) => (
              <View >
                <Card style={styles.card} >
                  <View >
                  <Image source={{ uri: item.url }} style={styles.image3}></Image>
                  <Text style={styles.text6}>{item.first_name}</Text>
                  <Text style={styles.text8}>{item.gender}</Text>  
                  <Text style={styles.text9}>Rating</Text>
                  <Text style={styles.text10}>Jobs</Text>
                  <Text style={styles.text11}>Rate</Text>
                  <Text style={styles.text12}>4.5</Text>
                  <Text style={styles.text13}>134</Text>
                  <Text style={styles.text14}>$10/hr</Text>
                  <Text style={styles.icon3}><Icon name='star-rate' size={20} /></Text>
                  </View>
                </Card>
              </View>
            )}
          />
        )}
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    padding: 10,
  },
  icon1: {
    left: 5,
    top: 2,
  },
  icon2: {
    marginLeft: 'auto',
    bottom: 55,
  },
  icon3:{
    left:105,
    bottom:178,
    color:'yellow'
  },
  text1: {
    left: 90,
    bottom: 25,
    fontSize: 20,
    fontWeight: 'bold',
    color: 'black'
  },
  text2: {
    fontSize: 20,
    fontWeight: 'bold',
    color: 'black',
    left:13
  },
  text3: {
    left: 280,
    bottom: 20
  },
  text4: {
    fontSize: 15,
    color: 'black',
    left: 40
  },
  text5: {
    fontSize: 15,
    color: 'black',
    left: 110
  },
  text6:{
    left:110,
    bottom:85,
    fontWeight:'bold',
    color:'black'
  },
  text8:{
    left:110,
    bottom:85,
    fontWeight:'bold',
    color:'red'
  },
  text9:{
    left:110,
    bottom:83,
  },
  text10:{
    left:200,
    bottom:100,
  },
   text11:{
    left:290,
    bottom:120,
  },
  text12:{
    left:130,
    bottom:120,
    color:'black',
    fontWeight:'bold'
  },
  text13:{
    left:200,
    bottom:138,
    color:'black',
    fontWeight:'bold'
  },
  text14:{
    left:280,
    bottom:155,
    color:'black',
    fontWeight:'bold'
  },
  image1: {
    height: 100,
    width: 140,
    left: 10,
    borderRadius: 10

  },
  image2: {
    height: 100,
    width: 140,
    left: 70,
    borderRadius: 10
  },
  image3: {
    height: 85,
    width: 85,
    borderRadius:5,
    left:13
    
  },

  one: {
    flexDirection: 'row',
    bottom: 35,
  },
  two: {
    flexDirection: 'row',
    bottom: 30,
  },
  three: {
    bottom: 25,
  },
  four:{
    bottom:30,
  },
   card:{
   height:90,
   margin:10
 }
});
export default App;